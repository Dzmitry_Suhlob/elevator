import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.log4j.PropertyConfigurator;

import by.epam.training.beans.Building;
import by.epam.training.beans.Elevator;
import by.epam.training.beans.Passenger;
import by.epam.training.beans.Story;
import by.epam.training.configs.Configurer;
import by.epam.training.constants.LogConstants;
import by.epam.training.threads.ElevatorController;
import by.epam.training.threads.TransportationTask;
 
public class Runner {	
	
	public static void main(String[] args) {
		
		Configurer reader = Configurer.getInstance();
		int storiesNumber = reader.getStoriesNumber();
		int elevatorCapacity = reader.getElevatorCapacity();
		int passengersNumber = reader.getPassengersNumber();
		
		List<Story> stories = new ArrayList<Story>();
		for(int i = 1; i <= storiesNumber; i++){
			stories.add(new Story(i));
		}
		
		Building building = new Building(stories);
		
		Elevator elevator = new Elevator(elevatorCapacity);
		
		int numDispatchStory;
		int numDestinationStory;
		Story dispatchStory;
		Story destinationStory;
		Random random = new Random();
		Passenger passenger;
		TransportationTask transportationTask;
		for(int i = 1; i <= passengersNumber; i++){
			numDispatchStory = Math.abs(random.nextInt(storiesNumber)) + 1;
			numDestinationStory = Math.abs(random.nextInt(storiesNumber)) + 1;
			if(numDestinationStory == numDispatchStory) {
				if(numDestinationStory == storiesNumber) {
					numDestinationStory--;
				} else {
					numDestinationStory++;
				}
			}
			dispatchStory = stories.get(numDispatchStory - 1);
			destinationStory = stories.get(numDestinationStory - 1);
			passenger = new Passenger (i, numDispatchStory, dispatchStory,
					numDestinationStory, destinationStory);
			stories.get(numDispatchStory - 1).getDispatchStoryContainer().add(passenger);
			
			synchronized(dispatchStory) {
				transportationTask = new TransportationTask(passenger, elevator);
				transportationTask.start();
			}
			
			while(true) {
				synchronized(dispatchStory) {
					if(transportationTask.isStarting()) {
						break;
					}
				}
			}
		}
		PropertyConfigurator.configure(LogConstants.logFileName);
		ElevatorController elevatorController = new ElevatorController(elevator, building);
		elevatorController.setNumLastStory(storiesNumber);	
		elevatorController.start();
		
	}

}
