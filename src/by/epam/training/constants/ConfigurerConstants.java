package by.epam.training.constants;

public class ConfigurerConstants {
	public final static String FILE_NAME = "config.properties"; 
	public final static String ELEVATOR_CAPACITY = "elevatorCapacity";
	public final static String PASSENGERS_NUMBER = "passengersNumber";
	public final static String STORIES_NUMBER = "storiesNumber";
}
