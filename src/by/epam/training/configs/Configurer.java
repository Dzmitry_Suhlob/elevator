package by.epam.training.configs;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import by.epam.training.constants.ConfigurerConstants;

public class Configurer {
	 
	private int storiesNumber;
	private int elevatorCapacity;
	private int passengersNumber;
	private static Configurer instance = new Configurer(ConfigurerConstants.FILE_NAME);
	
	private Configurer (String fileName) {
		Properties property = new Properties();
		try {
			property.load(new FileInputStream(fileName));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		storiesNumber = Integer.parseInt(property.getProperty(ConfigurerConstants.STORIES_NUMBER));
		elevatorCapacity = Integer.parseInt(property.getProperty(ConfigurerConstants.ELEVATOR_CAPACITY));
		passengersNumber = Integer.parseInt(property.getProperty(ConfigurerConstants.PASSENGERS_NUMBER));
	}
	
	public static Configurer getInstance() {
		return instance;
	}

	public int getStoriesNumber() {
		return storiesNumber;
	}

	public void setStoriesNumber(int storiesNumber) {
		this.storiesNumber = storiesNumber;
	}

	public int getElevatorCapacity() {
		return elevatorCapacity;
	}

	public void setElevatorCapacity(int elevatorCapacity) {
		this.elevatorCapacity = elevatorCapacity;
	}

	public int getPassengersNumber() {
		return passengersNumber;
	}

	public void setPassengersNumber(int passengersNumber) {
		this.passengersNumber = passengersNumber;
	}
}
