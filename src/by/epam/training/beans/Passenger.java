package by.epam.training.beans;

public class Passenger {
	 
	private final int passengerID;
	private final Story dispatchStory;
	private final Story destinationStory;
	private final int numDispatchStory;
	private final int numDestinationStory;
	private TransportationState transportationState;
	
	public Passenger(int passengerID, int numDispatchStory,
			Story dispatchStory, int numDestinationStory,
			Story destinationStory) {
		super();
		this.passengerID = passengerID;
		this.numDispatchStory = numDispatchStory;
		this.dispatchStory = dispatchStory;
		this.numDestinationStory = numDestinationStory;
		this.destinationStory = destinationStory;
	}

	public TransportationState getTransportationState() {
		return transportationState;
	}

	public void setTransportationState(TransportationState transportationState) {
		this.transportationState = transportationState;
	}

	public int getPassengerID() {
		return passengerID;
	}

	public Story getDispatchStory() {
		return dispatchStory;
	}

	public Story getDestinationStory() {
		return destinationStory;
	}

	public int getNumDispatchStory() {
		return numDispatchStory;
	}

	public int getNumDestinationStory() {
		return numDestinationStory;
	}

	public Direction getDirection() {
		if(numDispatchStory < numDestinationStory) {
			return Direction.UP;
		} else {
			return Direction.DOWN;
		}
	}
	
}
