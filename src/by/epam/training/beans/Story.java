package by.epam.training.beans;

import java.util.ArrayList;
import java.util.List;

public class Story {
	
	private final int storyID;
	private List<Passenger> dispatchStoryContainer = new ArrayList<Passenger>();
	private List<Passenger> arrivalStoryContainer = new ArrayList<Passenger>();
	
	public Story(int storyID) {
		super();
		this.storyID = storyID;
	} 

	public List<Passenger> getDispatchStoryContainer() {
		return dispatchStoryContainer;
	}

	public void setDispatchStoryContainer(List<Passenger> dispatchStoryContainer) {
		this.dispatchStoryContainer = dispatchStoryContainer;
	}

	public List<Passenger> getArrivalStoryContainer() {
		return arrivalStoryContainer;
	}

	public void setArrivalStoryContainer(List<Passenger> arrivalStoryContainer) {
		this.arrivalStoryContainer = arrivalStoryContainer;
	}

	public int getStoryID() {
		return storyID;
	}
	
}
