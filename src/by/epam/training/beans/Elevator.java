package by.epam.training.beans;

import java.util.ArrayList;
import java.util.List;

import by.epam.training.constants.ElevatorConstants;

public class Elevator { 
	
	private final List<Passenger> elevatorContainer;
	private final int elevatorCapacity;
	private int numStory;
	private int numPreviousStory;
	private Direction direction;
	
	public Elevator(int elevatorCapacity) {
		super();
		this.elevatorContainer = new ArrayList<Passenger>(elevatorCapacity);
		this.elevatorCapacity = elevatorCapacity;
		this.numStory = ElevatorConstants.START_STORY;
		this.direction = Direction.UP;
	}

	public int getNumStory() {
		return numStory;
	}

	public void setNumStory(int numStory) {
		this.numStory = numStory;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public List<Passenger> getElevatorContainer() {
		return elevatorContainer;
	}

	public int getNumFreePlaces() {
		return elevatorCapacity - elevatorContainer.size();
	}
	
	public synchronized int getNumDeboarding() {
		int numDeboarding = 0;
		for(Passenger p : elevatorContainer) {
			if(p.getNumDestinationStory() == numStory) {
				numDeboarding++;
			}
		}
		return numDeboarding;
	}

	public int getNumPreviousStory() {
		return numPreviousStory;
	}

	public void setNumPreviousStory(int numPreviousStory) {
		this.numPreviousStory = numPreviousStory;
	}
	
	

}
