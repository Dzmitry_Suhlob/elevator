package by.epam.training.beans;

import java.util.List;
 
public class Building {
	
	private List<Story> stories;

	public Building() {
		super();
	}

	public Building(List<Story> stories) {
		super();
		this.stories = stories;
	}

	public List<Story> getStories() {
		return stories;
	}

	public void setStories(List<Story> stories) {
		this.stories = stories;
	}

}
