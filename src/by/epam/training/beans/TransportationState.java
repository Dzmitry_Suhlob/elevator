package by.epam.training.beans;

public enum TransportationState {
	
	NOT_STARTED, IN_PROGRESS, COMPLETED, ABORTED

}
 