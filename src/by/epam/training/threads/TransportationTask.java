package by.epam.training.threads;

import java.util.List;

import org.apache.log4j.Logger;

import by.epam.training.beans.Elevator;
import by.epam.training.beans.Passenger;
import by.epam.training.beans.Story;
import by.epam.training.beans.TransportationState;

public class TransportationTask extends Thread {
	private static Logger log;
	private Passenger passenger;
	private Elevator elevator;
	private boolean starting;
	
	public TransportationTask(Passenger passenger, Elevator elevator) {
		log = Logger.getRootLogger();
		this.passenger = passenger;
		this.elevator = elevator;
	}
	
	public Passenger getPassenger() {
		return passenger;
	}

	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}

	public Elevator getElevator() {
		return elevator;
	}

	public void setElevator(Elevator elevator) {
		this.elevator = elevator;
	}

	public boolean isStarting() {
		return starting;
	}

	public void run() {
		try {
			Story dispatchStory = passenger.getDispatchStory();
			synchronized(dispatchStory) {
				passenger.setTransportationState(TransportationState.IN_PROGRESS);
				starting = true;
				dispatchStory.wait();
				while(true) {
					ElevatorController.increaseStoryNumThreadsActive();
					if(elevator.getNumFreePlaces() > 0) {
						if(elevator.getDirection().equals(passenger.getDirection())) {
							ElevatorController.boarding(passenger);
							log.info("BOARDING_OF_PASSENGER (passangerID#" + passenger.getPassengerID() +
									" on story#" + passenger.getNumDispatchStory() + ")");
							break;
						} else {
							dispatchStory.wait();
						}
					} else {
						dispatchStory.wait();
					}
				}
			}
			
			List<Passenger> elevatorContainer = elevator.getElevatorContainer();
			synchronized (elevatorContainer) {
				while(true) {
					synchronized(dispatchStory) {
						if(ElevatorController.isElevatorContainerStartingThreadsWaitingFlag()) {
							break;
						}
					}
				}
				ElevatorController.increaseElevatorContainerNumThreadsWaiting();
				if(ElevatorController.isElevatorContainerAllThreadsWaiting()) {
					ElevatorController.setElevatorContainerAllThreadsWaitingFlag(true);
				}
				elevatorContainer.wait();
				while(true) {
					if (elevator.getNumStory() == passenger.getNumDestinationStory()) {
						ElevatorController.decreaseElevatorContainerNumThreadsWaiting();
						ElevatorController.deboarding(passenger);
						log.info("DEBOARDING_OF_PASSENGER (passangerID#" + passenger.getPassengerID() +
								" on story#" + passenger.getNumDestinationStory() + ")");
						passenger.setTransportationState(TransportationState.COMPLETED);
						break;
					} else {
						elevatorContainer.wait();
					}
				}
				if(elevator.getNumDeboarding() == 0) {
					elevatorContainer.notifyAll();
				}
			}
		} catch (InterruptedException e) {
				e.printStackTrace();
		}
		
	}

}
