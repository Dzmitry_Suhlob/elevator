package by.epam.training.threads;

import java.util.List;

import org.apache.log4j.Logger;

import by.epam.training.beans.Building;
import by.epam.training.beans.Direction;
import by.epam.training.beans.Elevator;
import by.epam.training.beans.Passenger;
import by.epam.training.beans.Story;
import by.epam.training.beans.TransportationState;
import by.epam.training.constants.ElevatorConstants;

public class ElevatorController extends Thread {
	private static Logger log;
	private static Elevator elevator;
	private static Building building;
	private int numLastStory;
	private static  int storyNumThreadsActive;
	private static int elevatorContainerNumThreadsWaiting;
	private static boolean elevatorContainerAllThreadsWaitingFlag;
	private static boolean elevatorContainerStartingThreadsWaitingFlag;
	private boolean storyDirectionAllThreads;
	private boolean completionTransportation;
	
	public ElevatorController(Elevator elevator, Building building) {
		log = Logger.getRootLogger();
		ElevatorController.elevator = elevator;
		ElevatorController.building = building;
	}
	
	public static Elevator getElevator() {
		return elevator;
	}

	public static void setElevator(Elevator elevator) {
		ElevatorController.elevator = elevator;
	}

	public static Building getBuilding() {
		return building;
	}

	public static void setBuilding(Building building) {
		ElevatorController.building = building;
	}

	public int getNumLastStory() {
		return numLastStory;
	}

	public void setNumLastStory(int numLastStory) {
		this.numLastStory = numLastStory;
	}
	
	private static int getStoryNumThreadsActive() {
		return storyNumThreadsActive;
	}

	private static  void setStoryNumThreadsActive(int storyNumThreadsActive) {
		ElevatorController.storyNumThreadsActive = storyNumThreadsActive;
	}
	
	public static void increaseStoryNumThreadsActive() {
		int numThreadsActive = getStoryNumThreadsActive();
		numThreadsActive++;
		setStoryNumThreadsActive(numThreadsActive);
	}

	public static boolean isStoryAllThreadsActive(int numDepartures) {
		if(numDepartures == getStoryNumThreadsActive()) {
			return true;
		} else {
			return false;
		}
	}
	
	private static int getElevatorContainerNumThreadsWaiting() {
		return elevatorContainerNumThreadsWaiting;
	}

	private static void setElevatorContainerNumThreadsWaiting(
			int elevatorContainerNumThreadsWaiting) {
		ElevatorController.elevatorContainerNumThreadsWaiting = elevatorContainerNumThreadsWaiting;
	}
	
	public static void increaseElevatorContainerNumThreadsWaiting() {
		int numThreadsWaiting = getElevatorContainerNumThreadsWaiting();
		numThreadsWaiting++;
		setElevatorContainerNumThreadsWaiting(numThreadsWaiting);
	}
	
	public static void decreaseElevatorContainerNumThreadsWaiting() {
		int numThreadsWaiting = getElevatorContainerNumThreadsWaiting();
		numThreadsWaiting--;
		setElevatorContainerNumThreadsWaiting(numThreadsWaiting);
	}
	
	public static boolean isElevatorContainerAllThreadsWaiting() {
		int numAllThreads = elevator.getElevatorContainer().size();
		int numThreadsWaiting = getElevatorContainerNumThreadsWaiting();
		if(numThreadsWaiting == numAllThreads) {
			return true;
		} else {
			return false;
		}
	}

	public static void setElevatorContainerAllThreadsWaitingFlag(
			boolean elevatorContainerAllThreadsWaitingFlag) {
		ElevatorController.elevatorContainerAllThreadsWaitingFlag = elevatorContainerAllThreadsWaitingFlag;
	}

	public static boolean isElevatorContainerStartingThreadsWaitingFlag() {
		return elevatorContainerStartingThreadsWaitingFlag;
	}
	
	public int getNumDepartures() {
		int numDepartures = 0;
		Story story = building.getStories().get(elevator.getNumStory() - 1);
		numDepartures = story.getDispatchStoryContainer().size();
		return numDepartures;
	}

	public boolean isStoryDirectionAllThreads() {
		storyDirectionAllThreads = false;
		Story story = building.getStories().get(elevator.getNumStory() - 1);
		if(story.getDispatchStoryContainer().size() == 0) {
			return true;
		}
		for(Passenger p : story.getDispatchStoryContainer()) {
			if(p.getDirection().equals(elevator.getDirection())) {
				return true;
			}
		}
		return storyDirectionAllThreads;
	}

	public boolean isCompletionTransportation() {
		return completionTransportation;
	}

	public void moving() {
		int numStory = elevator.getNumStory();
		elevator.setNumPreviousStory(numStory);
		switch(elevator.getDirection()) {
		case UP: {
			numStory++;
			if(numStory == numLastStory) {
				elevator.setDirection(Direction.DOWN);
			}
			break;
		}
		case DOWN : {
			numStory--;
			if(numStory == ElevatorConstants.START_STORY) {
				elevator.setDirection(Direction.UP);
			}
			break;
		}
		}
		elevator.setNumStory(numStory);
	}
	
	private void boardingThread(Story story) {
		setStoryNumThreadsActive(0);
		elevatorContainerAllThreadsWaitingFlag = false;
		elevatorContainerStartingThreadsWaitingFlag = false;
		int numDepartures = getNumDepartures();
		synchronized(story) {
			story.notifyAll();
		}
		while(true) {
			synchronized(story) {
				if(isStoryAllThreadsActive(numDepartures)) {
					elevatorContainerStartingThreadsWaitingFlag = true;
					break;
				}
			}
		}
	}
	
	private void deboardingThread(Story story) {
		List<Passenger> elevatorContainer = elevator.getElevatorContainer();
		synchronized(elevatorContainer) {
			elevatorContainer.notifyAll();
			try {
				elevatorContainer.wait();
				waitElevatorContainerAllThreads();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void movingThread(Story story) {
		waitElevatorContainerAllThreads();
		moving();
		log.info("MOVING_ELEVATOR (from story#" + elevator.getNumPreviousStory() +
				" to story#" + elevator.getNumStory() + ")");
	}
	
	private void waitElevatorContainerAllThreads() {
		while(true) {
			synchronized(elevator.getElevatorContainer()) {
				if(elevatorContainerAllThreadsWaitingFlag) {
					break;
				}
			}
		}
	}
	
	public static boolean isProcess() {
		for(Story s : building.getStories()) {
			if(!s.getDispatchStoryContainer().isEmpty()) {
				return true;
			}
			if(!s.getArrivalStoryContainer().isEmpty()) {
				for(Passenger p : s.getArrivalStoryContainer()) {
					if(!p.getTransportationState().equals(TransportationState.COMPLETED)) {
						return true;
					}
					if(p.getNumDestinationStory() != s.getStoryID()) {
						return true;
					}
				}
			}
		}
		if(!elevator.getElevatorContainer().isEmpty()) {
			return true;
		}
		return false;
	}

	public static void boarding (Passenger passenger) {
		building.getStories().get(elevator.getNumStory() - 1).getDispatchStoryContainer().remove(passenger);
		elevator.getElevatorContainer().add(passenger);
	}
	
	public static void deboarding (Passenger passenger) {
		elevator.getElevatorContainer().remove(passenger);
		building.getStories().get(elevator.getNumStory() - 1).getArrivalStoryContainer().add(passenger);
	}
	
	@Override
	public void run() {
		log.info("STARTING_TRANSPORTATION");
		List<Passenger> elevatorContainer = elevator.getElevatorContainer();
		Story story;
		int numDeboarding;
		while(true) {
			story = building.getStories().get(elevator.getNumStory() - 1);
			if(!elevatorContainer.isEmpty()) {
				numDeboarding = elevator.getNumDeboarding();
				if(numDeboarding != 0) {
					deboardingThread(story);
				}
				if(elevator.getNumFreePlaces() != 0) {
					if(getNumDepartures() != 0 && isStoryDirectionAllThreads()) {
						boardingThread(story);
					}
				}
				if(elevatorContainer.isEmpty() && getNumDepartures()==0) {
					if(!isProcess()) {
						completionTransportation = true;
						log.info("COMPLETION_TRANSPORTATION");
						return;
					}
				}
				movingThread(story);
			} else {
				if(getNumDepartures() == 0) {
					elevatorContainerAllThreadsWaitingFlag = true;
					movingThread(story);
				} else {
					if(isStoryDirectionAllThreads()) {
						boardingThread(story);
					}
					movingThread(story);
				}
			}
			
		}
		
	}

}
