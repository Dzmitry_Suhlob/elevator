
1. The application is developed to transportation of passengers in the elevator.
2. Number of floors in house, elevator capacity, and number of the passengers determined by config.properties. Parameters for the application described as pairs key = value:
– storiesNumber – number of the stories in the building.
– elevatorCapacity – capacity of the elevator cabin.
– passengersNumber – number of passengers for transportation.
3. The passengers location on levels generates randomly . Passenger can’t get into the elevator, if there are no free seats or they are on the different floors.
4. Start the command prompt and change to the directory of the project Elevator (e.g. C:\workspace\elevator>).
5. At the command prompt, run the command “ant”.
6. There will be compiling and building the project, the transportation process starts with messages about the actions.
7. At the end of the transportation should see a message <COMPLETION_TRANSPORTATION>. This means that the process was successfully completed.
8. No passengers were harmed in doing this task. Two got stuck, and three seasick during the experiments.

